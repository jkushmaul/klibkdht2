#ifndef KDHT_CLI_H
#define KDHT_CLI_H

#include <kli.h>
#include <kdht_service.h>

void kdht_cli_init(kdht_service_t *dht, kli_t *cli, kli_com_t *dht_command);
#endif
