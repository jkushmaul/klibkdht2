#ifndef KDHT_RPC_FINDNODE_H
#define KDHT_RPC_FINDNODE_H

#include <klist.h>
#include <pb/kdht.pb-c.h>
#include <kdht_table.h>


typedef struct kdht_rpc_t kdht_rpc_t;
typedef struct kdht_find_node_t kdht_find_node_t;

typedef void (*kdht_rpc_find_node_call_complete)(kdht_rpc_t * rpc, kdht_find_node_t *call);

struct kdht_find_node_t {
	kdht_node_t target;
	RpcCookieIterativeCallType call_type;

	size_t closest_most_sig;
	klist_t shortlist;

	klist_t outstanding_contacts;

	uint64_t initiated_time;

	kdht_rpc_find_node_call_complete fn_complete;
};

int kdht_rpc_findnode_shortlist_contains(kdht_find_node_t *call, kdht_contact_t *target);
kdht_find_node_t *kdht_rpc_findnode_get_call(klist_t *find_node_calls, RpcCookieIterativeCallType call_type, kdht_node_t *target);
kdht_find_node_t *kdht_rpc_findnode_get_or_create_call_allocs(uint64_t current_time,
							      kdht_table_t *table,
							      kdht_node_t *requestor,
							      klist_t *find_node_calls,
							      RpcCookieIterativeCallType call_type,
							      kdht_node_t *target,
							      kdht_rpc_find_node_call_complete on_complete);
void kdht_rpc_findnode_shortlist_add_allocs(kdht_find_node_t *call, kdht_contact_t *target);

void kdht_findnode_call_release_frees(kdht_find_node_t *call);
int kdht_rpc_findnode_distancesortedlist_add_allocs(klist_t *list, kdht_node_t *target, kdht_contact_t *new_contact);
#endif
