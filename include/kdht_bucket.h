#ifndef KDHT_BUCKET_H
#define KDHT_BUCKET_H
#include <stdint.h>

#include <kindexedmap.h>

#include <kdht_node.h>

#define KDHT_CONTACT_EXPIRE_SECONDS 3610
#define KDHT_CONTACT_REFRESH_SECONDS 3600

typedef struct kdht_contact_t {
	kdht_node_t node;
	uint64_t last_updated_time;
	char *network_address;
}kdht_contact_t;

typedef struct kdht_bucket_t {
	kindexedmap_t contacts_map;
	uint64_t last_refresh_time;
} kdht_bucket_t;


void kdht_contact_release_frees(kdht_contact_t *contact);
void *kdht_contact_new_allocs(kdht_node_t *node, char *network_address);
int kdht_contact_equals(kdht_contact_t *left, kdht_contact_t *right);

kdht_contact_t* kdht_bucket_get_contact(kdht_bucket_t *bucket, kdht_node_t *search);
void kdht_bucket_insert_contact_allocs(kdht_bucket_t *bucket, kdht_contact_t* contact);

int kdht_bucket_collect_k_nodes_allocs(kdht_bucket_t *bucket, kdht_node_t *this_node, kdht_node_t *requestor, klist_t* nnd_list);

void kdht_bucket_release_frees(kdht_bucket_t *bucket);

void kdht_bucket_init_allocs(kdht_bucket_t *bucket);

#endif
