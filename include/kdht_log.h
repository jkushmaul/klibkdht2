#ifndef KDHT_LOG_H
#define KDHT_LOG_H

typedef enum  {
	DEBUG = 0,
	INFO,
	WARN,
	ERROR
} klog_level;

void klog_msg(klog_level level, const char *message, ...);

#endif

