#ifndef KDHT_RPC_H
#define KDHT_RPC_H
#include <stdint.h>
#include <stdlib.h>
#include <pb/kdht.pb-c.h>
#include <kservice.h>
#include <kdht_table.h>
#include <kdht_rpc_findnode.h>

#define KDHT_RPC_MAX_WAIT_LEN 4096
#define KDHT_MAX_COOKIE_LEN 256
#define KDHT_MAX_VALUE_BYTES 4096

typedef struct kdht_rpc_t {
	//TODO: remove need for service with just copy of network address, or make it an argument, current time also needed.
	kservice_t service;
	kdht_table_t table;
	kdht_contact_t contact_information; //public address and node
	uint16_t max_ttl_seconds;
	//one list to hold all outstanding iterative find_value, find_node, and store_value (which all use find_node)
	klist_t find_node_calls;

	kindexedmap_t outstanding_findvalues;
	khashmap_t *found_values_answers;
} kdht_rpc_t;

//used to avoid malloc and extra args to inits
typedef struct kdht_protobuf_message {
	RpcMessage message;
	struct  _header {
		RpcMessageHeader container;
		RpcContact src;
		uint8_t cookiebuffer[KDHT_MAX_COOKIE_LEN];
		struct _cookie {
			RpcCookie container;
			RpcContact contact;
			char network_address[512];
			kdht_node_t node;
			RpcCookieIterativeCall iterative_call;
			kdht_node_t target_node;
		} decryptedCookie;

	} header;

	RpcMessageProcedures procedure;
	struct _procedures {
		struct _ping {
			RpcPing container;
			RpcPing__Request request;
			RpcPing__Response response;
			uint8_t pingdata[KDHT_MAX_COOKIE_LEN];
			size_t pingdata_len;
		} ping;
		struct _findnode {
			RpcFindNode container;
			RpcFindNode__Request request;
			RpcFindNode__Response response;
			size_t nnd_count;
			RpcContact nnds[DHT_K];
			RpcContact *nnds_ptrs[DHT_K];
		} findnode;
		struct _findvalue {
			RpcFindValue container;
			RpcFindValue__Request request;
			RpcFindValue__Response response;
		} findvalue;
		struct _storevalue {
			RpcStoreValue container;
			RpcStoreValue__Request request;
			RpcStoreValue__Response response;
			kdht_node_t key;
			size_t data_len;
			uint8_t data[KDHT_MAX_VALUE_BYTES];
		} storevalue;
	} procedures;
	uint8_t header_id_data[DHT_ID_BYTES];
	char destination_network_address[2048];
} kdht_protobuf_message;

void kdht_rpc_init(kdht_rpc_t *rpc);
void kdht_rpc_release_frees(kdht_rpc_t *rpc);

//message
RpcCookie *kdht_rpc_decrypt_cookie_allocs(RpcMessage *message);
int kdht_rpc_init_message(kdht_rpc_t * rpc, int is_response, kdht_contact_t * contact, kdht_protobuf_message * init);
int kdht_rpc_validate_message(kdht_rpc_t * rpc, RpcMessage *message);
int kdht_rpc_handle_message(kdht_rpc_t * rpc, kdht_contact_t *src_contact,
			    RpcMessage *message, kdht_protobuf_message *responses, size_t max_responses);
int kdht_rpc_handle_response(kdht_rpc_t * rpc, kdht_contact_t *src_contact,
			     RpcMessage *message, kdht_protobuf_message *responses, size_t max_responses);
int kdht_rpc_handle_request(kdht_rpc_t * rpc, kdht_contact_t *src_contact,
			    RpcMessage *message, kdht_protobuf_message *responses, size_t max_responses);


//ping
void kdht_rpc_init_ping_reponse(kdht_rpc_t * rpc, kdht_contact_t *src_contact, kdht_protobuf_message *init);
void kdht_rpc_init_ping_request(kdht_rpc_t * rpc, kdht_contact_t *src_contact, kdht_protobuf_message *init);
int kdht_rpc_handle_ping_request(kdht_rpc_t * rpc, kdht_contact_t *src_contact,
				 const RpcMessage *message, kdht_protobuf_message *response);

//find_node
void kdht_rpc_init_findnode_request(kdht_rpc_t * rpc, kdht_find_node_t *call, kdht_protobuf_message *init,
				    kdht_contact_t *src_contact, kdht_node_t *target, int bootstrapping, int iterative, RpcCookieIterativeCallType call_type);
void kdht_rpc_init_findnode_response(kdht_rpc_t * rpc,  kdht_protobuf_message *init,  kdht_contact_t *src_contact);
void kdht_findnode_populate_nnd(kdht_rpc_t * rpc, kdht_node_t *key, kdht_contact_t *src_contact, kdht_protobuf_message *response);
int kdht_rpc_findnode_handle_request(kdht_rpc_t * rpc, kdht_contact_t *src_contact,
				     const RpcMessage *message, kdht_protobuf_message *response);
int kdht_rpc_findnode_handle_response_allocs(kdht_rpc_t * rpc, kdht_contact_t *src_contact,
					     const RpcMessage *message, const RpcCookie *cookie, kdht_protobuf_message *outbounds, int max_outbounds);
int kdht_rpc_findnode_handle_iterative_nnd(kdht_rpc_t * rpc, kdht_find_node_t *call, kdht_contact_t *src_contact, const RpcCookie *cookie,
					   size_t n_contacts, RpcContact **contacts, kdht_protobuf_message *outbounds, size_t max_outbounds);

int kdht_rpc_get_alpha_closest_from_nnd(kdht_rpc_t * rpc, kdht_node_t *target, size_t n_contacts, RpcContact **contacts, size_t storage_len, RpcContact **storage);

int kdht_rpc_start_iterative_find_node(kdht_rpc_t * rpc, kdht_node_t *target, kdht_protobuf_message *outbound, size_t max_outbound);



void kdht_rpc_init_findvalue_request(kdht_rpc_t * rpc, kdht_find_node_t *call, kdht_protobuf_message *init, kdht_contact_t *src_contact, kdht_node_t *target);
void kdht_rpc_init_findvalue_response(kdht_rpc_t * rpc, kdht_protobuf_message *init,  kdht_contact_t *src_contact, kdht_value_t *value);
void kdht_rpc_init_storevalue_request(kdht_rpc_t * rpc,  kdht_protobuf_message *init,  kdht_contact_t *src_contact, kdht_value_t *value, int iterative);
void kdht_rpc_init_storevalue_response(kdht_rpc_t * rpc,  kdht_protobuf_message *init, kdht_contact_t *src_contact, kdht_value_t *value);
int kdht_rpc_storevalue_handle_request_allocs(kdht_rpc_t * rpc, kdht_contact_t *src_contact, const RpcMessage *message,
					      kdht_protobuf_message *response);
int kdht_rpc_storevalue_handle_response(kdht_rpc_t * rpc, kdht_contact_t *src_contact, const RpcMessage *message,
					kdht_protobuf_message *response);

void kdht_rpc_execute_andrelease_findnode_frees(kdht_rpc_t *rpc, kdht_find_node_t *call);

int kdht_rpc_findvalue_handle_request(kdht_rpc_t * rpc, kdht_contact_t *src_contact, const RpcMessage *message, kdht_protobuf_message *response);
int kdht_rpc_findvalue_handle_response(kdht_rpc_t * rpc, kdht_contact_t *src_contact, const RpcMessage *message, kdht_protobuf_message *response);
#endif
