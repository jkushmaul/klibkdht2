#include <stddef.h>
#include <string.h>
#include <stdio.h>

#include <kdht_rpc.h>
#include <kdht_log.h>

int compare_value_time(void *left, void *right)
{
	kdht_value_t *cleft = (kdht_value_t*)left;
	kdht_value_t *cright = (kdht_value_t*)right;

	return cleft->expire_time - cright->expire_time;
}

void kdht_rpc_init(kdht_rpc_t *rpc)
{
	kdht_node_copy(&rpc->contact_information.node, &rpc->table.current_node);

	kindexedmap_init_allocs(&rpc->outstanding_findvalues, compare_value_time, 1000);
	rpc->found_values_answers = khashmap_new_allocs(1000);

	kdht_table_init_allocs(&rpc->table);
	for (int i = 0; i < KDHT_TABLE_T_ELEMENTS; i++) {
		rpc->table.routing_table[i].last_refresh_time = rpc->service.timestamp;
	}

}
void kdht_rpc_release_frees(kdht_rpc_t *rpc)
{
	while (rpc->find_node_calls.head) {
		klist_node_t *this = rpc->find_node_calls.head;
		klist_remove_node(&rpc->find_node_calls, this);
		kdht_find_node_t *call = (kdht_find_node_t*)this->value;
		kdht_findnode_call_release_frees(call);
		free(call);
		free(this);
	}

	kdht_table_release_frees(&rpc->table);

	khashmap_entry_t *entry = NULL;
	KHASHMAP_FOR_EACH_VALUE(rpc->found_values_answers, entry) {
		kdht_value_t *value = entry->value;

		free(value->bytes);
		free(value);
	}
	KLIST_FOR_EACH_NODE(node, &rpc->outstanding_findvalues.index.list) {
		kdht_value_t *value = node->value;

		free(value->bytes);
		free(value);
	}

	khashmap_release_frees(rpc->found_values_answers);
	kindexedmap_release_frees(&rpc->outstanding_findvalues);
}

/**
 * @brief Given the message, find the rpc and execute it.
 * The executor may return 0 to indicate success without response, or 1 for success with response.
 *
 * @param dht p_dht:...
 * @param client p_client:...
 * @param srcnode p_srcnode:...
 * @param message p_message:...
 * @param response p_response:...
 * @return int | <0 on error, 0 on success without response, 1 on success with response
 */
int kdht_rpc_handle_message(kdht_rpc_t * rpc, kdht_contact_t *src_contact, RpcMessage *message,
			    kdht_protobuf_message *response, size_t max_responses)
{
	int result = 0;

	if (message->header->cookie.len == 0 || message->header->cookie.len > KDHT_MAX_COOKIE_LEN) {
		klog_msg(ERROR, "kdht_rcp_handle_request received invalid cookie length");
		return -1;
	}

	//if it is a response
	if (message->header->is_response) {
		result = kdht_rpc_handle_response(rpc, src_contact, message, response, max_responses);
	} else {
		result = kdht_rpc_handle_request(rpc, src_contact, message, response, max_responses);
	}

	if (result >= 0) {
		//any successful incoming message gets an update to the contact
		kdht_node_t *n = (kdht_node_t*)message->header->src->id.data;
		kdht_contact_t *contact = kdht_table_find_contact(&rpc->table, n);
		if (contact == NULL) {
			contact = kdht_contact_new_allocs(n, message->header->src->addr);
		}

		kdht_table_update_contact_allocs_frees(rpc->service.timestamp, &rpc->table, contact);
	}

	return result;
}

int kdht_rpc_handle_response(kdht_rpc_t * rpc, kdht_contact_t *src_contact, RpcMessage *message, kdht_protobuf_message *responses, size_t max_responses)
{
	if (message->header->cookie.len == 0 || message->header->cookie.len > KDHT_MAX_COOKIE_LEN) {
		klog_msg(ERROR, "kdht_rcp_handle_response received invalid cookie length");
		return -1;
	}

	RpcCookie *cookie = rpc_cookie__unpack(NULL, message->header->cookie.len, message->header->cookie.data);

	int result = 0;
	if (!cookie) {
		klog_msg(ERROR, "kdht_rcp_handle_response received invalid cookie data from %s",  src_contact->network_address);
		result = -2;
	} else if (message->procedure->ping) {
		klog_msg(INFO, "Received ping response from %s",  src_contact->network_address);
	} else if (message->procedure->findnode) {
		klog_msg(INFO, "Received findnode response from %s",  src_contact->network_address);
		result = kdht_rpc_findnode_handle_response_allocs(rpc, src_contact, message, cookie, responses, max_responses);
	} else if (message->procedure->findvalue) {
		klog_msg(INFO, "Received findvalue response from %s",  src_contact->network_address);
		result = kdht_rpc_findvalue_handle_response(rpc, src_contact, message, responses);
	} else if (message->procedure->storevalue) {
		klog_msg(INFO, "Received storevalue response from %s", src_contact->network_address);
		result = kdht_rpc_storevalue_handle_response(rpc, src_contact, message, responses);
	} else {
		klog_msg(ERROR, "kdht_rcp_handle_response received invalid message procedure");
		result = -4;
	}

	if (cookie) {
		rpc_cookie__free_unpacked(cookie, NULL);
	}

	return result;
}

int kdht_rpc_handle_request(kdht_rpc_t * rpc, kdht_contact_t *src_contact, RpcMessage *message,
			    kdht_protobuf_message *response, size_t max_responses)
{
	if (message->header->cookie.len == 0 || message->header->cookie.len > KDHT_MAX_COOKIE_LEN) {
		klog_msg(ERROR, "kdht_rcp_handle_request received invalid cookie length");
		return -1;
	}

	int result = 0;

	if (message->procedure->ping) {
		klog_msg(INFO, "Received ping request from %s",  src_contact->network_address);
		result = kdht_rpc_handle_ping_request(rpc, src_contact, message, response);
	} else if (message->procedure->findnode) {
		klog_msg(INFO, "Received findnode request from %s",  src_contact->network_address);
		result = kdht_rpc_findnode_handle_request(rpc, src_contact, message, response);
	} else if (message->procedure->findvalue) {
		klog_msg(INFO, "Received findvalue request from %s",  src_contact->network_address);
		result = kdht_rpc_findvalue_handle_request(rpc, src_contact, message, response);
	} else if (message->procedure->storevalue) {
		klog_msg(INFO, "Received storevalue request from %s",  src_contact->network_address);
		result = kdht_rpc_storevalue_handle_request_allocs(rpc, src_contact, message, response);
	} else {
		klog_msg(ERROR, "kdht_rcp_handle_request received invalid message procedure");
		result = -4;
	}

	if (result > 0) {
		response->message.header->cookie.len = message->header->cookie.len;
		memcpy(response->header.cookiebuffer, message->header->cookie.data, message->header->cookie.len);
	}

	return result;
}

int kdht_rpc_validate_message(kdht_rpc_t * rpc, RpcMessage *message)
{
	if (message == NULL) {
		klog_msg(ERROR, "kdht_service_handle_buffered_message received invalid message");
		return -1;
	}
	if (message->header == NULL) {
		klog_msg(ERROR, "kdht_service_handle_buffered_message received invalid message header");
		return -2;
	}
	if (message->header->src->id.data == 0 || message->header->src->id.len != DHT_ID_BYTES) {
		klog_msg(ERROR, "kdht_service_handle_buffered_message received invalid message header with invalid id");
		return -2;
	}
	if (message->header->src == NULL) {
		klog_msg(ERROR, "kdht_service_handle_buffered_message received invalid message header with null src");
		return -2;
	}
	if (message->header->cookie.len == 0 || message->header->cookie.len >= KDHT_MAX_COOKIE_LEN) {
		klog_msg(ERROR, "kdht_service_handle_buffered_message received invalid cookie len in header");
		return -2;
	}
	//TODO: Provide network address compare to knet.
	if (!strcmp(rpc->contact_information.network_address, message->header->src->addr)) {
		klog_msg(ERROR, "kdht_service_handle_buffered_message received a message from our own public remote");
		return -11;
	}
	if (!kdht_node_equals(&rpc->table.current_node, (kdht_node_t*)message->header->src->id.data)) {
		klog_msg(ERROR, "kdht_service_handle_buffered_message received a message from our own node id");
		return -10;
	}
	if (message->procedure == NULL) {
		klog_msg(ERROR, "kdht_service_handle_buffered_message received invalid message procedure");
		return -3;
	}
	//ensure cookie can be decrypted and matches checksum
	return 0;
}

int kdht_rpc_init_message(kdht_rpc_t * rpc, int is_response, kdht_contact_t *contact, kdht_protobuf_message *init)
{
	memset(init, 0, sizeof(kdht_protobuf_message));
	rpc_message__init(&init->message);
	rpc_message_header__init(&init->header.container);
	init->message.header = &init->header.container;
	init->header.container.cookie.data = init->header.cookiebuffer;

	if (!is_response) {
		rpc_cookie__init(&init->header.decryptedCookie.container);
		RpcCookie *cookie = &init->header.decryptedCookie.container;
		cookie->contact = &init->header.decryptedCookie.contact;
		rpc_contact__init(cookie->contact);
		rpc_cookie_iterative_call__init(&init->header.decryptedCookie.iterative_call);
		cookie->timestmap = rpc->service.timestamp;
		cookie->contact->addr = init->header.decryptedCookie.network_address;
		snprintf(cookie->contact->addr, 512, "%s",  contact->network_address);
		kdht_node_copy(&contact->node, &init->header.decryptedCookie.node);
		cookie->contact->id.data = init->header.decryptedCookie.node.id;
		cookie->contact->id.len = DHT_ID_BYTES;
	}

	init->header.container.is_response = is_response;
	init->header.container.src = &init->header.src;
	rpc_contact__init(init->header.container.src);
	init->header.container.src->addr = rpc->contact_information.network_address;
	init->header.container.src->id.len = DHT_ID_BYTES;
	init->header.container.src->id.data = init->header_id_data;
	memcpy(init->header_id_data, rpc->table.current_node.id, DHT_ID_BYTES);

	rpc_message_procedures__init(&init->procedure);
	init->message.procedure = &init->procedure;

	return 0;
}
///////////////////////////////////////RPCs////////////////////////////////

////////////PING//////////////
void kdht_rpc_init_ping_request(kdht_rpc_t * rpc, kdht_contact_t *src_contact, kdht_protobuf_message *init)
{
	klog_msg(DEBUG, "kdht_service_init_ping_request");
	kdht_rpc_init_message(rpc, 0, src_contact, init);
	RpcMessage *message = &init->message;
	//just send response
	message->procedure->ping = &init->procedures.ping.container;
	rpc_ping__init(message->procedure->ping);
	message->procedure->ping->request = &init->procedures.ping.request;
	rpc_ping__request__init(message->procedure->ping->request);
	message->procedure->ping->request->data.data = init->procedures.ping.pingdata;
}
void kdht_rpc_init_ping_reponse(kdht_rpc_t * rpc,  kdht_contact_t *src_contact, kdht_protobuf_message *init)
{
	klog_msg(DEBUG, "kdht_service_init_ping_reponse");
	kdht_rpc_init_message(rpc, 1, src_contact, init);
	RpcMessage *message = &init->message;

	//just send response
	message->procedure->ping = &init->procedures.ping.container;
	rpc_ping__init(message->procedure->ping);
	message->procedure->ping->response = &init->procedures.ping.response;
	rpc_ping__response__init(message->procedure->ping->response);
}

/**
 * @brief Handles ping req/resp
 *
 * @param dht p_dht:...
 * @param client p_client:...
 * @param message p_message:...
 * @param response p_response:...
 * @return int | < 0 on failure, 0 success no response, 1 success with response
 */
int kdht_rpc_handle_ping_request(kdht_rpc_t * rpc, kdht_contact_t *src_contact, const RpcMessage *message, kdht_protobuf_message *response)
{
	klog_msg(DEBUG, "kdht_rpc_handle_ping_request");

	RpcPing *ping = message->procedure->ping;

	klog_msg(DEBUG, "kdht_rpc_handle_ping_request from %s", src_contact->network_address);
	if (ping == NULL || ping->request == NULL) {
		klog_msg(ERROR, "kdht_rpc_handle_ping received NULL ping request");
		return -1;
	}
	kdht_rpc_init_ping_reponse(rpc, src_contact, response);
	strncpy(response->destination_network_address, src_contact->network_address, sizeof(response->destination_network_address));
	return 1;
}

////////////FIND_NODE//////////////

void kdht_rpc_init_findnode_request(kdht_rpc_t* rpc, kdht_find_node_t* call, kdht_protobuf_message* init, kdht_contact_t* src_contact, kdht_node_t* target, int bootstrapping, int iterative, RpcCookieIterativeCallType call_type)
{
	klog_msg(DEBUG, "kdht_rpc_init_findnode_request");
	kdht_rpc_init_message(rpc, 0, src_contact, init);

	RpcMessage *message = &init->message;
	RpcFindNode *findnode = &init->procedures.findnode.container;
	rpc_find_node__init(findnode);
	message->procedure->findnode = findnode;

	RpcFindNode__Request *request = &init->procedures.findnode.request;
	rpc_find_node__request__init(request);
	message->procedure->findnode->request = request;

	message->procedure->findnode->request->key.len = DHT_ID_BYTES;
	message->procedure->findnode->request->key.data = src_contact->node.id;

	kdht_node_copy(target, &init->header.decryptedCookie.target_node);
	init->header.decryptedCookie.container.bootstrapping = bootstrapping;
	init->header.decryptedCookie.container.iterative_call = &init->header.decryptedCookie.iterative_call;
	init->header.decryptedCookie.container.iterative_call->iterative = iterative;
	init->header.decryptedCookie.container.iterative_call->call_type = call_type;
	init->header.decryptedCookie.container.iterative_call->target_node.data = init->header.decryptedCookie.target_node.id;
	init->header.decryptedCookie.container.iterative_call->target_node.len = DHT_ID_BYTES;

	if (call) {
		klist_insert_value_allocs(&call->outstanding_contacts, NULL, strdup(src_contact->network_address));
	}
}


void kdht_rpc_init_findnode_response(kdht_rpc_t * rpc, kdht_protobuf_message *init,  kdht_contact_t *src_contact)
{
	klog_msg(DEBUG, "kdht_service_init_findnode_response");
	kdht_rpc_init_message(rpc, 1, src_contact, init);

	RpcMessage *message = &init->message;

	RpcFindNode *findnode = &init->procedures.findnode.container;
	rpc_find_node__init(findnode);
	message->procedure->findnode = findnode;

	RpcFindNode__Response *response = &init->procedures.findnode.response;
	rpc_find_node__response__init(response);
	findnode->response = response;
}

void kdht_findnode_init_nnd(kdht_contact_t *contact, RpcContact *dst)
{
	rpc_contact__init(dst);

	dst->id.data = contact->node.id;
	dst->id.len = DHT_ID_BYTES;
	dst->addr = contact->network_address;
}

void kdht_findnode_populate_nnd(kdht_rpc_t * rpc, kdht_node_t *key, kdht_contact_t *src_contact, kdht_protobuf_message *response)
{
	//1 get a list of contacts
	klist_t nnd_list;

	memset(&nnd_list, 0, sizeof(klist_t));
	kdht_table_find_n_close_nodes_allocs(&rpc->table, &src_contact->node, key, DHT_K, &nnd_list);

	response->procedures.findnode.nnd_count = nnd_list.count;
	memset(response->procedures.findnode.nnds, 0, sizeof(response->procedures.findnode.nnds));

	klog_msg(DEBUG, "kdht_findnode_populate_nnd: closest_nodes=%d", nnd_list.count);
	//2. Convert to array
	int i = 0;
	kdht_node_string_t nodestr;
	memset(&nodestr, 0, sizeof(kdht_node_string_t ));

	RpcContact *nnd_array = response->procedures.findnode.nnds;
	RpcContact **nnd_ptr_array = response->procedures.findnode.nnds_ptrs;

	//i want to take whats in nnd_list
	//and put it in the find node response contacts.

	while (nnd_list.head != NULL && i < DHT_K) {
		klist_node_t *node = nnd_list.tail;
		kdht_contact_t *contact = node->value;
		klist_remove_node(&nnd_list, node);
		free(node);

		//do not return a node its own
		if (!kdht_node_equals(&src_contact->node, &contact->node) || !strcmp(src_contact->network_address, contact->network_address)) {
			continue;
		}
		nnd_ptr_array[i] = &nnd_array[i];
		kdht_findnode_init_nnd(contact, &nnd_array[i]);


		kdht_node_tostring(&contact->node, &nodestr);
		klog_msg(DEBUG, "kdht_findnode_populate_nnd: nnd[%d]=%s@%s", i, nnd_array[i].addr, nodestr.id);

		response->procedure.findnode->response->n_contacts = i + 1;
		i++;
	}
	if (response->procedure.findnode->response->n_contacts) {
		response->procedure.findnode->response->contacts = nnd_ptr_array;
	}
}

int kdht_rpc_findnode_handle_request(kdht_rpc_t * rpc, kdht_contact_t *src_contact, const RpcMessage *message,
				     kdht_protobuf_message *response)
{
	klog_msg(DEBUG, "kdht_service_findnode_handle_request");
	if (message->procedure->findnode == NULL || message->procedure->findnode->request == NULL) {
		klog_msg(ERROR, "kdht_service_findnode_handle_request received null findnode request");
		return -1;
	}
	RpcFindNode__Request *fn_request = message->procedure->findnode->request;

	kdht_node_t key;
	if (fn_request->key.len != DHT_ID_BYTES) {
		klog_msg(ERROR, "Received request with invalid id size");
		return -1;
	}
	memcpy(&key, fn_request->key.data, DHT_ID_BYTES);

	kdht_rpc_init_findnode_response(rpc, response, src_contact);
	klog_msg(DEBUG, "destination_network_address = %s", src_contact->network_address);
	strncpy(response->destination_network_address, src_contact->network_address, sizeof(response->destination_network_address));
	kdht_findnode_populate_nnd(rpc, &key, src_contact, response);

	return 1;
}

int kdht_rpc_findnode_handle_response_allocs(kdht_rpc_t * rpc, kdht_contact_t *src_contact, const RpcMessage *message,
					     const RpcCookie *cookie, kdht_protobuf_message *outbounds, int max_outbounds)
{
	klog_msg(DEBUG, "kdht_service_findnode_handle_response");
	if (message->procedure->findnode == NULL || message->procedure->findnode->response == NULL) {
		klog_msg(ERROR, "kdht_service_findnode_handle_response received null findnode response");
		return -1;
	}

	if (cookie->iterative_call == NULL || cookie->iterative_call->target_node.len != DHT_ID_BYTES) {
		klog_msg(DEBUG, "kdht_rpc_findnode_handle_iterative_nnd received invalid cookie find_node_id");
		return -1;
	}

	kdht_node_t *target = (kdht_node_t*)cookie->iterative_call->target_node.data;

	kdht_find_node_t *call = kdht_rpc_findnode_get_call(&rpc->find_node_calls, cookie->iterative_call->call_type, target);
	if (call == NULL) {
		klog_msg(DEBUG, "kdht_rpc_findnode_handle_response_allocs received unknown cookie find_node_id");
		return -1;
	}

	RpcFindNode__Response *fn_response = message->procedure->findnode->response;

	kdht_node_string_t current_node_str;
	kdht_node_tostring(&rpc->contact_information.node, &current_node_str);

	klog_msg(DEBUG, "kdht_service_findnode_handle_response received %d from %s", fn_response->n_contacts,  src_contact->network_address);
	for (int i = 0; i < fn_response->n_contacts; i++) {
		RpcContact *c = fn_response->contacts[i];
		if (c->addr == NULL || c->id.data == NULL || c->id.len != DHT_ID_BYTES) {
			klog_msg(DEBUG, "kdht_service_findnode_handle_response received invalid contact[%d] from %s", i,  src_contact->network_address);
			return -1;
		}
	}
	int outbound_count = 0;
	if (cookie->iterative_call->iterative) {
		outbound_count = kdht_rpc_findnode_handle_iterative_nnd(rpc, call, src_contact, cookie, fn_response->n_contacts, fn_response->contacts, outbounds, max_outbounds);
	}

	return outbound_count;
}


kdht_contact_t *kdht_rpc_nnd_to_contact(RpcContact *nnd)
{
	if (nnd->id.len != DHT_ID_BYTES) {
		return NULL;
	}
	if (nnd->addr == NULL) {
		return NULL;
	}

	return kdht_contact_new_allocs((kdht_node_t*)nnd->id.data, nnd->addr);
}

int kdht_rpc_sort_nnd(kdht_rpc_t * rpc, kdht_node_t *target, klist_t *sorted_list, size_t n_contacts, RpcContact **contacts)
{
	//for each node, calculate distance between target and that node
	//store sorted closest first in storage up to storage_len
	if (n_contacts == 0) {
		return 0;
	}

	for (size_t i = 0; i < n_contacts; i++) {
		RpcContact *c = contacts[i];
		if (c->id.len != DHT_ID_BYTES) {
			return -2;
		} else if (c->addr == NULL) {
			return -3;
		} else if (!kdht_node_equals(&rpc->table.current_node, (kdht_node_t*)c->id.data)) {
			continue;
		}

		kdht_contact_t *new_contact = kdht_rpc_nnd_to_contact(c);
		//can't use sortedlist here because it requires extra context due to how distance is calculated.
		kdht_rpc_findnode_distancesortedlist_add_allocs(sorted_list, target, new_contact);
	}
	return 0;
}

int klist_find_by_strcmp(void *left, void *right)
{
	return strcmp(left, right);
}


//TODO: break this up  to smaller functions
int kdht_rpc_findnode_handle_iterative_nnd(kdht_rpc_t * rpc, kdht_find_node_t *call, kdht_contact_t *src_contact, const RpcCookie *cookie, size_t n_contacts, RpcContact **contacts, kdht_protobuf_message *outbounds, size_t max_outbounds)
{
	klog_msg(DEBUG, "kdht_rpc_findnode_handle_iterative_nnd received %d", n_contacts);

	//First, this should have been an outstanding call.
	klist_node_t *node = klist_find_node_by_comparison(&call->outstanding_contacts, src_contact->network_address, klist_find_by_strcmp);
	if (node != NULL) {
		free(node->value);
		klist_remove_node(&call->outstanding_contacts, node);
		free(node);
	} else {
		klog_msg(DEBUG, "kdht_rpc_findnode_handle_iterative_nnd unsolicited outstanding call, could not find call");
		klog_msg(DEBUG, "kdht_rpc_findnode_handle_iterative_nnd: src_contact->network_address=%s, call->outstanding_contacts.count=%d", src_contact->network_address, call->outstanding_contacts.count);
		return -1;
	}

	kdht_contact_t *head = NULL;
	if (call->shortlist.head) {
		head = call->shortlist.head->value;
	}


	size_t outbound_count = 0;
	if (n_contacts > 0) {

		klist_t sorted_nnd;
		klist_init(&sorted_nnd);
		kdht_rpc_sort_nnd(rpc, &call->target, &sorted_nnd, n_contacts, contacts);
		//Iterate sorted by distance RpcContacts
		while (sorted_nnd.head != NULL) {
			klist_node_t *tmp_node = sorted_nnd.head;
            kdht_contact_t *contact = tmp_node->value;
            klist_remove_node(&sorted_nnd, tmp_node);
            free(tmp_node);
			if (kdht_rpc_findnode_shortlist_contains(call, contact)) {
				kdht_contact_release_frees(contact);
				continue;
			}

			kdht_node_string_t node_str;
			kdht_node_tostring(&contact->node, &node_str);
			klog_msg(DEBUG, "kdht_service_findnode_handle_response adding %s@%s\n", node_str.id, contact->network_address);
			kdht_rpc_findnode_shortlist_add_allocs(call, contact);

			//create iterative outbound.
			/**
			            if (call->call_type == RPC_COOKIE_ITERATIVE_CALL_TYPE__ITERATIVE_FIND_VALUE) {
			                    kdht_rpc_init_findvalue_request(rpc, call, &outbounds[outbound_count], contact, &call->target);
			            } else {
			                    kdht_rpc_init_findnode_request(rpc, call, &outbounds[outbound_count], contact, &call->target, cookie->bootstrapping, 1, call->call_type);
			            }**/
			kdht_rpc_init_findnode_request(rpc, call, &outbounds[outbound_count], contact, &call->target, cookie->bootstrapping, 1, call->call_type);

			strncpy(outbounds[outbound_count].destination_network_address, contact->network_address, sizeof(outbounds[outbound_count].destination_network_address));
			outbound_count++;
		}
	}


	if (call->shortlist.head != NULL && call->shortlist.head->value != head) {
		//if we keep finding new nodes that hit the head of the queue, we are not done.
		return outbound_count;
	} else {
		//nothing was closer.
		if (call->outstanding_contacts.count == 0) {
			kdht_rpc_execute_andrelease_findnode_frees(rpc, call);
			return 0;
		}
		return 0;
	}
}
void kdht_rpc_execute_andrelease_findnode_frees(kdht_rpc_t *rpc, kdht_find_node_t *call)
{
	kdht_node_string_t nodestr;

	kdht_node_tostring(&call->target, &nodestr);
	klog_msg(DEBUG, "kdht_service_gc_outstanding_calls: finished for %s", nodestr.id);

	if (call->fn_complete) {
		call->fn_complete(rpc, call);
	}

	klist_node_t *node = klist_find_node_by_value_address(&rpc->find_node_calls, call);
	klist_remove_node(&rpc->find_node_calls, node);
	kdht_findnode_call_release_frees(call);
	free(call);
	free(node);
}

////////////VALUES//////////////
void kdht_rpc_init_storevalue_request(kdht_rpc_t * rpc,  kdht_protobuf_message *init,  kdht_contact_t *src_contact, kdht_value_t *value, int iterative)
{
	klog_msg(DEBUG, "kdht_rpc_init_storevalue_request");

	kdht_rpc_init_message(rpc, 0, src_contact, init);

	RpcMessage *message = &init->message;
	RpcStoreValue *storevalue = &init->procedures.storevalue.container;
	rpc_store_value__init(storevalue);
	message->procedure->storevalue = storevalue;

	RpcStoreValue__Request *request = &init->procedures.storevalue.request;
	rpc_store_value__request__init(request);
	message->procedure->storevalue->request = request;

	request->data.len = value->len;
	request->data.data = init->procedures.storevalue.data;
	memcpy(request->data.data, value->bytes, value->len);

	request->key.len = DHT_ID_BYTES;
	request->key.data = init->procedures.storevalue.key.id;
	kdht_node_copy(&value->key, (kdht_node_t*)request->key.data);
	request->ttl = value->expire_time;

	init->header.decryptedCookie.container.iterative_call = &init->header.decryptedCookie.iterative_call;
	init->header.decryptedCookie.container.iterative_call->iterative = iterative;
	init->header.decryptedCookie.container.iterative_call->call_type = RPC_COOKIE_ITERATIVE_CALL_TYPE__ITERATIVE_STORE_VALUE;
	init->header.decryptedCookie.container.iterative_call->target_node.data = init->header.decryptedCookie.target_node.id;
	init->header.decryptedCookie.container.iterative_call->target_node.len = DHT_ID_BYTES;

}

void kdht_rpc_init_storevalue_response(kdht_rpc_t * rpc,  kdht_protobuf_message *init, kdht_contact_t *src_contact, kdht_value_t *value)
{
	klog_msg(DEBUG, "kdht_rpc_init_findnode_request");
	kdht_rpc_init_message(rpc, 1, src_contact, init);

	RpcMessage *message = &init->message;
	RpcStoreValue *storevalue = &init->procedures.storevalue.container;
	rpc_store_value__init(storevalue);
	message->procedure->storevalue = storevalue;

	RpcStoreValue__Response *response = &init->procedures.storevalue.response;
	rpc_store_value__response__init(response);
	message->procedure->storevalue->response = response;

	response->key.len = DHT_ID_BYTES;
	response->key.data = init->procedures.storevalue.key.id;
	kdht_node_copy(&value->key, (kdht_node_t*)response->key.data);
}

int kdht_rpc_storevalue_handle_request_allocs(kdht_rpc_t * rpc, kdht_contact_t *src_contact, const RpcMessage *message,
					      kdht_protobuf_message *response)
{
	klog_msg(DEBUG, "kdht_rpc_storevalue_handle_request");
	if (message->procedure->storevalue == NULL || message->procedure->storevalue->request == NULL) {
		klog_msg(ERROR, "kdht_rpc_storevalue_handle_request received null request");
		return -1;
	}
	RpcStoreValue__Request *fn_request = message->procedure->storevalue->request;

	if (fn_request->key.len != DHT_ID_BYTES) {
		klog_msg(ERROR, "kdht_rpc_storevalue_handle_request received invalid value key");
		return -1;
	}
	if (fn_request->ttl > rpc->service.timestamp + rpc->max_ttl_seconds) {
		klog_msg(ERROR, "kdht_rpc_storevalue_handle_request ttl breaches max ttl");
		return -1;
	}
	if (fn_request->data.len > KDHT_MAX_VALUE_BYTES) {
		klog_msg(ERROR, "kdht_rpc_storevalue_handle_request data length breaches max len allowed");
		return -1;
	}
	kdht_value_t *requested_value = malloc(sizeof(kdht_value_t));
	memset(requested_value, 0, sizeof(kdht_value_t));
	kdht_node_copy((kdht_node_t*)fn_request->key.data, &requested_value->key);
	requested_value->expire_time = fn_request->ttl;
	requested_value->len = fn_request->data.len;
	requested_value->bytes = malloc(requested_value->len);
	memcpy(requested_value->bytes, fn_request->data.data, requested_value->len);
	requested_value->last_replicate_time = rpc->service.timestamp;
	kdht_value_t *pushedout = kdht_table_insert_value(&rpc->table.remote_values, requested_value);
	if (pushedout) {
		if (pushedout->bytes) {
			free(pushedout->bytes);
		}
		free(pushedout);
	}

	kdht_rpc_init_storevalue_response(rpc, response, src_contact, requested_value);
	strncpy(response->destination_network_address, src_contact->network_address, sizeof(response->destination_network_address));

	return 1;
}


int kdht_rpc_storevalue_handle_response(kdht_rpc_t * rpc, kdht_contact_t *src_contact, const RpcMessage *message,
					kdht_protobuf_message *response)
{
	klog_msg(DEBUG, "kdht_rpc_storevalue_handle_response");
	if (message->procedure->storevalue == NULL || message->procedure->storevalue->response == NULL) {
		klog_msg(ERROR, "kdht_rpc_storevalue_handle_response received null response");
		return -1;
	}
	RpcStoreValue__Response *fn_response = message->procedure->storevalue->response;

	if (fn_response->key.len != DHT_ID_BYTES) {
		klog_msg(ERROR, "kdht_rpc_storevalue_handle_response received invalid value key");
		return -1;
	}

	kdht_node_string_t node_str;
	kdht_node_tostring((kdht_node_t*)fn_response->key.data, &node_str);
	klog_msg(ERROR, "kdht_rpc_storevalue_handle_response received response, value %s sucessfully stored", node_str.id);

	return 0;
}


void kdht_rpc_init_findvalue_request(kdht_rpc_t * rpc, kdht_find_node_t *call, kdht_protobuf_message *init, kdht_contact_t *src_contact, kdht_node_t *target)
{
	klog_msg(DEBUG, "kdht_rpc_init_findvalue_request");
	kdht_rpc_init_message(rpc, 0, src_contact, init);

	RpcMessage *message = &init->message;
	RpcFindValue *findvalue = &init->procedures.findvalue.container;
	rpc_find_value__init(findvalue);
	message->procedure->findvalue = findvalue;

	RpcFindValue__Request *request = &init->procedures.findvalue.request;
	rpc_find_value__request__init(request);
	message->procedure->findvalue->request = request;

	message->procedure->findvalue->request->key.len = DHT_ID_BYTES;
	message->procedure->findvalue->request->key.data = target->id;


	kdht_node_copy(target, &init->header.decryptedCookie.target_node);
	init->header.decryptedCookie.container.iterative_call = &init->header.decryptedCookie.iterative_call;
	init->header.decryptedCookie.container.iterative_call->iterative = 1;
	init->header.decryptedCookie.container.iterative_call->call_type = RPC_COOKIE_ITERATIVE_CALL_TYPE__ITERATIVE_FIND_VALUE;
	init->header.decryptedCookie.container.iterative_call->target_node.data = init->header.decryptedCookie.target_node.id;
	init->header.decryptedCookie.container.iterative_call->target_node.len = DHT_ID_BYTES;

	if (call) {
		klist_insert_value_allocs(&call->outstanding_contacts, NULL, strdup(src_contact->network_address));
	}
}


void kdht_rpc_init_findvalue_response(kdht_rpc_t * rpc, kdht_protobuf_message *init,  kdht_contact_t *src_contact, kdht_value_t *value)
{
	klog_msg(DEBUG, "kdht_rpc_init_findvalue_response");
	kdht_rpc_init_message(rpc, 1, src_contact, init);

	RpcMessage *message = &init->message;

	RpcFindValue *findvalue = &init->procedures.findvalue.container;
	rpc_find_value__init(findvalue);
	message->procedure->findvalue = findvalue;


	RpcFindValue__Response *response = &init->procedures.findvalue.response;
	rpc_find_value__response__init(response);
	findvalue->response = response;

	response->key.data = value->key.id;
	response->key.len = DHT_ID_BYTES;
	response->has_data = 1;
	response->data.data = value->bytes;
	response->data.len = value->len;
}


int kdht_rpc_findvalue_handle_request(kdht_rpc_t * rpc, kdht_contact_t *src_contact, const RpcMessage *message, kdht_protobuf_message *response)
{
	klog_msg(DEBUG, "kdht_rpc_findvalue_handle_request");
	if (message->procedure->findvalue == NULL || message->procedure->findvalue->request == NULL) {
		klog_msg(ERROR, "kdht_rpc_findvalue_handle_request received null request");
		return -1;
	}
	RpcFindValue__Request *fn_request = message->procedure->findvalue->request;

	if (fn_request->key.len != DHT_ID_BYTES) {
		klog_msg(ERROR, "kdht_rpc_findvalue_handle_request received invalid value key");
		return -1;
	}
	kdht_node_string_t node_str = { 0 };
	kdht_node_tostring((kdht_node_t*)fn_request->key.data, &node_str);
	klog_msg(DEBUG, "kdht_rpc_findvalue_handle_request with value.key = %s", node_str.id);


	klist_node_t *lnode = kdht_table_get_value(&rpc->table.remote_values, (kdht_node_t*)fn_request->key.data);
	if (lnode != NULL) {
		kdht_value_t *value = lnode->value;
		kdht_rpc_init_findvalue_response(rpc, response, src_contact, value);
		strncpy(response->destination_network_address, src_contact->network_address, sizeof(response->destination_network_address));
		return 1;
	}

	return 0;
}

int kdht_rpc_findvalue_handle_response(kdht_rpc_t * rpc, kdht_contact_t *src_contact, const RpcMessage *message, kdht_protobuf_message *response)
{
	klog_msg(DEBUG, "kdht_rpc_findvalue_handle_response");

	if (message->procedure->findvalue == NULL || message->procedure->findvalue->response == NULL) {
		klog_msg(ERROR, "kdht_rpc_findvalue_handle_response received null response");
		return -1;
	}
	RpcFindValue__Response *fn_response = message->procedure->findvalue->response;

	if (fn_response->key.len != DHT_ID_BYTES) {
		klog_msg(ERROR, "kdht_rpc_findvalue_handle_response received invalid value key");
		return -1;
	}

	khashmap_key_t key = { 0 };
	key.data = fn_response->key.data;
	key.len = fn_response->key.len;
	kdht_value_t *val = kindexedmap_remove_frees(&rpc->outstanding_findvalues, &key);
	if (val == NULL) {
		klog_msg(ERROR, "kdht_rpc_findvalue_handle_response received value response for non-existing value key (or was already answered)");
		return -1;
	} else {
		key.data = val->key.id;
		key.len = DHT_ID_BYTES;
		khashmap_put_allocs(rpc->found_values_answers, &key, val);
		val->bytes = calloc(1, fn_response->data.len);
		val->len = fn_response->data.len;
		memcpy(val->bytes, fn_response->data.data, fn_response->data.len);
	}
	return 0;
}
