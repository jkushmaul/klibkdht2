#include <stdlib.h>
#include <malloc.h>
#include <string.h>

#include <kdht_bucket.h>


void *kdht_contact_new_allocs(kdht_node_t *node, char *network_address)
{
	kdht_contact_t *contact = calloc(1, sizeof(kdht_contact_t));

	contact->network_address = strdup(network_address);
	kdht_node_copy(node, &contact->node);
	return contact;
}
void kdht_contact_release_frees(kdht_contact_t *contact)
{
	if (contact->network_address) {
		free(contact->network_address);
	}
	free(contact);
}

int contact_compare_time(void *left, void *right)
{
	kdht_contact_t *cleft = (kdht_contact_t*)left;
	kdht_contact_t *cright = (kdht_contact_t*)right;

	return cleft->last_updated_time - cright->last_updated_time;
}

void kdht_bucket_init_allocs(kdht_bucket_t *bucket)
{
	memset(bucket, 0, sizeof(kdht_bucket_t));
	kindexedmap_init_allocs(&bucket->contacts_map, contact_compare_time, 1000);
}

void kdht_bucket_release_frees(kdht_bucket_t *bucket)
{

	while (bucket->contacts_map.index.list.head != NULL) {
		klist_node_t *node = bucket->contacts_map.index.list.head;
		kdht_contact_t *contact = (kdht_contact_t*)node->value;
		khashmap_key_t key = { 0 };
		key.data = contact->node.id;
		key.len = sizeof(kdht_node_t);
		kindexedmap_remove_frees(&bucket->contacts_map, &key);
		kdht_contact_release_frees(contact);
	}

	kindexedmap_release_frees(&bucket->contacts_map);
}

kdht_contact_t *kdht_bucket_get_contact(kdht_bucket_t *bucket, kdht_node_t *search)
{
	khashmap_key_t key = { 0 };

	key.data = search->id;
	key.len = sizeof(kdht_node_t);
	return (kdht_contact_t*)kindexedmap_get(&bucket->contacts_map, &key);
}

int kdht_bucket_collect_k_nodes_allocs(kdht_bucket_t *bucket, kdht_node_t *this_node, kdht_node_t *requestor, klist_t* nnd_list)
{
	KLIST_FOR_EACH_NODE(node, &bucket->contacts_map.index.list) {
		kdht_contact_t *c = (kdht_contact_t*)node->value;

		if (kdht_node_equals(requestor, &c->node)) {
			klist_insert_value_allocs(nnd_list, NULL, c);
		}
		if (nnd_list->count >= DHT_K) {
			break;
		}
	}
	return nnd_list->count;;
}

int kdht_contact_equals(kdht_contact_t *left, kdht_contact_t *right)
{
	if (kdht_node_equals(&left->node, &right->node)) {
		return 1;
	} else if (strcmp(left->network_address, right->network_address)) {
		return 2;
	} else {
		return 0;
	}
}

klist_node_t *kdht_contact_find_contact_list_node(klist_t *list, kdht_contact_t *contact)
{
	KLIST_FOR_EACH_NODE(node, list) {
		kdht_contact_t *c = (kdht_contact_t*)node->value;

		if (!kdht_contact_equals(contact, c)) {
			return node;
		}
	}
	return NULL;
}


void kdht_bucket_insert_contact_allocs(kdht_bucket_t *bucket, kdht_contact_t* contact)
{
	khashmap_key_t key = { 0 };

	key.data = contact->node.id;
	key.len = sizeof(kdht_node_t);
	kdht_contact_t *orig_value = (kdht_contact_t*)kindexedmap_put_allocs(&bucket->contacts_map, &key, contact);
	if (orig_value != NULL && orig_value != contact) {
		//the count will not have changed
		kdht_contact_release_frees(orig_value);
	} else if (bucket->contacts_map.index.list.count > DHT_K) {
		klist_node_t *entry = bucket->contacts_map.index.list.head;
		orig_value = (kdht_contact_t*)entry->value;
		memset(&key, 0, sizeof(khashmap_key_t));
		key.data = orig_value->node.id;
		key.len = sizeof(kdht_node_t);
		kindexedmap_remove_frees(&bucket->contacts_map, &key);
		kdht_contact_release_frees(orig_value);
	}
}
