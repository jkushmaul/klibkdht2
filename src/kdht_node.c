#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <time.h>

#include <kencode.h>
#include <kdht_node.h>

void kdht_node_copy(kdht_node_t * src, kdht_node_t * dst)
{
	memcpy(dst, src, sizeof(kdht_node_t));
}

int kdht_node_equals(kdht_node_t * src, kdht_node_t * dst)
{
	return memcmp(src, dst, sizeof(kdht_node_t));
}

void kdht_node_tostring(kdht_node_t  *src, kdht_node_string_t *dst)
{
	memset(dst->id, 0, sizeof(dst->id));
	kencode_hex_encode(src->id, sizeof(src->id), dst->id, sizeof(dst->id) - 1);
}

int kdht_node_parse(kdht_node_string_t *src, kdht_node_t *dst)
{
	size_t slen = strlen(src->id);

	if (slen != sizeof(src->id) - 1) {
		return -1;
	}

	return kencode_hex_decode(src->id, slen, dst->id, sizeof(kdht_node_t));
}

static uint8_t __kdht_byte_most_sig_bit_pos(uint8_t byte)
{
	uint8_t bit = 0x80;//starts at 10000000

	for (int i = 0; i < 8; i++ ) {
		if (bit & byte) {
			return i;
		}
		bit >>= 1;//shifts bit by 1 to right
	}
	return 7;
}

uint16_t kdht_node_most_sig_bit_pos(kdht_node_t  *dist)
{
	uint16_t index = DHT_BITS - 1;

	FOR_EACH_DHT_ID_BYTE(i) {
		if (dist->id[i] > 0) {
			uint8_t bit = __kdht_byte_most_sig_bit_pos(dist->id[i]);
			index = i * 8 + bit;
			break;
		}
	}
	return index;
}

void kdht_node_calculate_distance(kdht_node_t  *left, kdht_node_t  *right, kdht_node_t  *result)
{
	FOR_EACH_DHT_ID_BYTE(i) {
		result->id[i] = left->id[i] ^ right->id[i];
	}
}

static int random_seed = 0;

void kdht_node_random(kdht_node_t *dst)
{
	if (!random_seed) {
		//yeah yeah
		srand(time(NULL));
		random_seed = 1;
	}
	FOR_EACH_DHT_ID_BYTE(i) {
		dst->id[i] = rand() % 0xff;
	}
}
