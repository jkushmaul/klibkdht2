#include <stdio.h>
#include <string.h>

#include <kencode.h>
#include "kdht_cli.h"




int print_bucket_information(kdht_service_t *service, int start, int stop)
{
	for (int index = start; index < stop; index++) {
		klist_t *bucket = &service->rpc.table.routing_table[index].contacts_map.index.list;
		int i = 0;
		if (bucket->count == 0) {
			continue;
		}
		printf("bucket[%d]: %d nodes\n", index, bucket->count);
		printf("%-15s", "contact");
		printf("%-40s", "netaddr");
		printf("%20s%5s", "last_updated", "");

		printf("%s", "id");
		printf("\n");
		KLIST_FOR_EACH_NODE(node, bucket) {
			kdht_contact_t *c = (kdht_contact_t*)node->value;

			kdht_node_string_t nodestr;

			kdht_node_tostring(&c->node, &nodestr);
			char buff[30];
			snprintf(buff, sizeof(buff), "contact[%03d]",  i++);
			printf("%-15s", buff);
			printf("%-40s", c->network_address);
			printf("%20ld%5s", c->last_updated_time, "");
			printf("%s", nodestr.id);
			printf("\n");
		}
	}
	return 0;
}

int kdht_cli_show_buckets(kli_t* cli, void *vcontext, kli_exec_t *exec)
{
	kdht_service_t *service = (kdht_service_t*)vcontext;
	uint32_t total = 0;
	kdht_node_string_t this_node_str;

	kdht_node_tostring(&service->rpc.contact_information.node, &this_node_str);
	char * this_node_network = service->rpc.contact_information.network_address;

	printf("This node: %s@%s\n", this_node_str.id, this_node_network);
	printf("Total contacts: %u\n", total);
	printf("%-15s", "table");
	printf("%8s", "contacts");
	printf("\n");

	for (int i = 0; i < KDHT_TABLE_T_ELEMENTS; i++) {
		klist_t *bucket = &service->rpc.table.routing_table[i].contacts_map.index.list;
		total += bucket->count;
		if (bucket->count > 0) {
			print_bucket_information(service, i, i + 1);
		}
	}
	return 0;
}


int kdht_cli_show_bucket_i(kli_t* cli, void *vcontext, kli_exec_t *exec)
{
	if (exec->argc != 1) {
		printf("One argument required\n");
		return -1;
	}

	kdht_service_t *service = (kdht_service_t*)vcontext;
	int showall = strcasecmp(exec->argv[0], "*") == 0;
	int start, stop;
	if (showall) {
		start = 0;
		stop = KDHT_TABLE_T_ELEMENTS;
	} else {
		int len = strspn(exec->argv[0], "0123456789");
		if (len != strlen(exec->argv[0])) {
			printf("numeric argument required\n");
			return -1;
		}
		start = atoi(exec->argv[0]);
		stop = start + 1;
		if (start < 0 || start > KDHT_TABLE_T_ELEMENTS - 1) {
			printf("numeric argument must be [0,%d]\n", KDHT_TABLE_T_ELEMENTS);
			return -1;
		}
	}


	for (int index = start; index < stop; index++) {
		klist_t *bucket = &service->rpc.table.routing_table[index].contacts_map.index.list;
		int i = 0;
		if (bucket->count == 0) {
			continue;
		}
		printf("bucket[%d]: %d nodes\n", index, bucket->count);
		printf("%-15s", "contact");
		printf("%-40s", "netaddr");
		printf("%20s%5s", "last_updated", "");

		printf("%s", "id");
		printf("\n");
		KLIST_FOR_EACH_NODE(node, bucket) {
			kdht_contact_t *c = (kdht_contact_t*)node->value;

			kdht_node_string_t nodestr;

			kdht_node_tostring(&c->node, &nodestr);
			char buff[30];
			snprintf(buff, sizeof(buff), "contact[%03d]",  i++);
			printf("%-15s", buff);
			printf("%-40s", c->network_address);
			printf("%20ld%5s", c->last_updated_time, "");
			printf("%s", nodestr.id);
			printf("\n");
		}
	}
	return 0;
}

int kdht_cli_show_values_local(kli_t *cli, void *vcontext, kli_exec_t *exec)
{
	kdht_service_t *service = (kdht_service_t*)vcontext;
	klist_t *values = &service->rpc.table.local_values;
	int i = 0;

	printf("value     ttl\trepublish\tnodeid\tsize\n");
	printf("Total values: %d\n", i);
	printf("%-15s", "value");
	printf("%20s", "ttl");
	printf("%20s", "republish");
	printf("%*s", DHT_BITS * 2, "id");
	printf("%15s", "size");
	KLIST_FOR_EACH_NODE(node, values) {
		kdht_value_t *v = (kdht_value_t*)node->value;
		kdht_node_string_t nodestr;

		kdht_node_tostring(&v->key, &nodestr);

		char buff[30];
		snprintf(buff, sizeof(buff), "value[%03d]",  i++);
		printf("%-15s", buff);
		printf("%-20ld", v->expire_time);
		printf("%20ld", v->last_replicate_time);
		printf("%*s", DHT_BITS * 2, nodestr.id);
		printf("%15u", v->len);
		printf("\n");
	}

	return 0;
}


int kdht_cli_show_values_remote(kli_t *cli, void *vcontext, kli_exec_t *exec)
{
	kdht_service_t *service = (kdht_service_t*)vcontext;
	klist_t *values = &service->rpc.table.remote_values;
	int i = 0;

	printf("Total values: %d\n", i);
	printf("%-15s", "value");
	printf("%20s", "ttl");
	printf("%20s", "republish");
	printf("%*s", DHT_BITS * 2, "id");
	printf("%15s", "size");
	KLIST_FOR_EACH_NODE(node, values) {
		kdht_value_t *v = (kdht_value_t*)node->value;
		kdht_node_string_t nodestr;

		kdht_node_tostring(&v->key, &nodestr);

		char buff[30];
		snprintf(buff, sizeof(buff), "value[%03d]",  i++);
		printf("%-15s", buff);
		printf("%-20ld", v->expire_time);
		printf("%20ld", v->last_replicate_time);
		printf("%*s", DHT_BITS * 2, nodestr.id);
		printf("%15u", v->len);
		printf("\n");
	}
	printf("Total values: %d\n", i);
	return 0;
}



int kdht_cli_store_value_str(kli_t *cli, void *vcontext, kli_exec_t *exec)
{
	kdht_service_t *service = (kdht_service_t*)vcontext;

	if (exec->argc != 3) {
		printf("STORE_VALUE <host> <key_hex> <datahex>\n");
		return -1;
	}
	char *host = exec->argv[0];
	char *id = exec->argv[1];
	char *hexdata = exec->argv[2];

	kdht_node_t key;
	if (strlen(id) != DHT_ID_BYTES * 2) {
		printf("Invalid key format\n");
		return -1;
	}
	if (kdht_node_parse((kdht_node_string_t*)id, &key)) {
		printf("invalid key format\n");
		return -1;
	}
	int len = strcspn(hexdata, "abcdef0123456789");
	if (len != 0 || len % 2 != 0) {
		printf("Invalid hex data\n");
		return -1;
	}

	size_t datalen = strlen(hexdata) / 2;
	uint8_t* data = malloc(datalen);
	kencode_hex_decode(hexdata, strlen(hexdata), data, datalen);
	kdht_value_t *value = malloc(sizeof(kdht_value_t));
	memset(value, 0, sizeof(kdht_value_t));
	kdht_node_copy(&key, &value->key);
	value->len = datalen;
	value->bytes = data;

	int val = kdht_service_send_store_value(service, host, value, 1);
	if (val <= 0) {
		free(data);
		free(value);
	}
	return val;
}


void kdht_cli_find_value_completed(kdht_rpc_t *rpc, kdht_find_node_t *call)
{
	printf("FIND_VALUE completed\n");
}

int kdht_cli_find_value_str(kli_t* cli, void *vcontext, kli_exec_t *exec)
{
	kdht_service_t *service = (kdht_service_t*)vcontext;

	if (exec->argc != 2) {
		printf("FIND_VALUE <IP> <key_hex>\n");
		return -1;
	}


	if (strlen(exec->argv[1]) != DHT_ID_BYTES * 2) {
		printf("invalid key length\n");
		return -1;
	}

	kdht_node_string_t *node_str = (kdht_node_string_t*)exec->argv[1];
	kdht_node_t target;
	memset(&target, 0, sizeof(kdht_node_t));
	if (kdht_node_parse(node_str, &target)) {
		printf("invalid key format\n");
		return -1;
	}

	kdht_find_node_t *call = kdht_rpc_findnode_get_or_create_call_allocs(service->rpc.service.timestamp,
									     &service->rpc.table,
									     &service->rpc.contact_information.node,
									     &service->rpc.find_node_calls,
									     RPC_COOKIE_ITERATIVE_CALL_TYPE__ITERATIVE_FIND_VALUE,
									     &service->rpc.contact_information.node,
									     kdht_cli_find_value_completed);


	int val = kdht_service_send_findvalue(service, exec->argv[0], &target, 0, call);

	return val;
}

void kdht_cli_find_node_completed(kdht_rpc_t *rpc, kdht_find_node_t *call)
{
	printf("FIND_NODE completed\n");
}

int kdht_cli_find_node_str(kli_t* cli, void *vcontext, kli_exec_t *exec)
{
	kdht_service_t *service = (kdht_service_t*)vcontext;

	if (exec->argc != 2) {
		printf("FIND_NODE <IP> <key_hex>\n");
		return -1;
	}


	if (strlen(exec->argv[1]) != DHT_ID_BYTES * 2) {
		printf("invalid key length\n");
		return -1;
	}

	kdht_node_string_t *node_str = (kdht_node_string_t*)exec->argv[1];
	kdht_node_t target;
	memset(&target, 0, sizeof(kdht_node_t));
	if (kdht_node_parse(node_str, &target)) {
		printf("invalid key format\n");
		return -1;
	}

	kdht_find_node_t *call = kdht_rpc_findnode_get_or_create_call_allocs(service->rpc.service.timestamp,
									     &service->rpc.table,
									     &service->rpc.contact_information.node,
									     &service->rpc.find_node_calls,
									     RPC_COOKIE_ITERATIVE_CALL_TYPE__ITERATIVE_FIND_NODE,
									     &service->rpc.contact_information.node,
									     kdht_cli_find_node_completed);

	int val = kdht_service_find_node_send_request(service, call, exec->argv[0], node_str, 0, 0, RPC_COOKIE_ITERATIVE_CALL_TYPE__ITERATIVE_FIND_NODE);
	return val;
}

void kdht_cli_find_node_iterative_completed(kdht_rpc_t *rpc, kdht_find_node_t *call)
{
	printf("ITERATIVE_FIND_NODE completed\n");
}

int kdht_cli_find_node_iterative_str(kli_t* cli, void *vcontext, kli_exec_t *exec)
{
	kdht_service_t *service = (kdht_service_t*)vcontext;

	if (exec->argc != 1) {
		printf("ITERATIVE_FIND_NODE <key_hex>\n");
		return -1;
	}

	if (strlen(exec->argv[1]) != DHT_ID_BYTES * 2) {
		printf("invalid key format\n");
		return -1;
	}

	kdht_node_string_t *node_str = (kdht_node_string_t*)exec->argv[1];
	kdht_node_t target;
	memset(&target, 0, sizeof(kdht_node_t));
	if (kdht_node_parse(node_str, &target)) {
		printf("invalid key format\n");
		return -1;
	}

	kdht_find_node_t *call = kdht_rpc_findnode_get_or_create_call_allocs(
		service->rpc.service.timestamp,
		&service->rpc.table,
		&service->rpc.contact_information.node,
		&service->rpc.find_node_calls,
		RPC_COOKIE_ITERATIVE_CALL_TYPE__ITERATIVE_FIND_NODE,
		&service->rpc.contact_information.node,
		kdht_cli_find_node_iterative_completed );

	int val = kdht_service_iterative_find_node(service, &target, 0, call);
	return val;
}

int kdht_cli_find_value_iterative_str(kli_t* cli, void *vcontext, kli_exec_t *exec)
{
	kdht_service_t *service = (kdht_service_t*)vcontext;

	if (exec->argc != 1) {
		printf("ITERATIVE_FIND_VALUE <key_hex>\n");
		return -1;
	}
	char *key = exec->argv[0];

	if (strlen(key) != DHT_ID_BYTES * 2) {
		printf("invalid key format\n");
		return -1;
	}

	kdht_node_string_t *node_str = (kdht_node_string_t*)key;
	kdht_node_t target;
	memset(&target, 0, sizeof(kdht_node_t));
	if (kdht_node_parse(node_str, &target)) {
		printf("invalid key format\n");
		return -1;
	}

	int val = kdht_service_iterative_findvalue(service, &target);
	return val;
}


int kdht_cli_store_value_iterative_str(kli_t* cli, void *vcontext, kli_exec_t *exec)
{
	kdht_service_t *service = (kdht_service_t*)vcontext;

	if (exec->argc != 2) {
		printf("ITERATIVE_STORE_VALUE <key_hex> <value_hex>\n");
		return -1;
	}

	char *id = exec->argv[0];
	char *hexdata = exec->argv[1];

	if (strlen(id) != DHT_ID_BYTES * 2) {
		printf("invalid key format\n");
		return -1;
	}
	int len = strcspn(hexdata, "abcdef0123456789");
	if (len != 0 || len % 2 != 0) {
		printf("Invalid hex data\n");
		return -1;
	}

	kdht_node_string_t *node_str = (kdht_node_string_t*)id;
	kdht_node_t target;
	memset(&target, 0, sizeof(kdht_node_t));
	if (kdht_node_parse(node_str, &target)) {
		printf("invalid key format\n");
		return -1;
	}

	size_t datalen = strlen(hexdata) / 2;
	uint8_t* data = malloc(datalen);
	kencode_hex_decode(hexdata, strlen(hexdata), data, datalen);
	kdht_value_t *value = malloc(sizeof(kdht_value_t));
	memset(value, 0, sizeof(kdht_value_t));
	kdht_node_copy(&target, &value->key);
	value->len = datalen;
	value->bytes = data;

	int val = kdht_service_iterative_store_value(service, value);
	return val;
}

int kdht_cli_show_active_net(kli_t* cli, void *vcontext, kli_exec_t *exec)
{
	kdht_service_t *service = (kdht_service_t*)vcontext;

	int i = 0;

	printf("Total clients: %zu\n", service->rpc.service.clients->total_entries);
	printf("%-15s", "client");
	printf("%20s", "netaddr");
	printf("\n");
	khashmap_entry_t *entry = NULL;
	KHASHMAP_FOR_EACH_VALUE(service->rpc.service.clients, entry) {
		kdht_service_client_t *c = (kdht_service_client_t*)entry->value;

		printf("%-15d", i++);
		printf("%20s", c->remote);
		printf("\n");
	}
	return 0;
}


int kdht_cli_ping_remote_str(kli_t* cli, void *vcontext, kli_exec_t* exec)
{
	if (exec->argc != 1) {
		printf("ping net <host:ip>");
	}
	kdht_service_t *service = (kdht_service_t*)vcontext;
	const char *host = exec->argv[0];
	kdht_service_ping_send_request(service, host);

	return 0;
}

int kdht_cli_node_expire(kli_t* cli, void *vcontext, kli_exec_t* exec)
{
	kdht_service_t *service = (kdht_service_t*)vcontext;

	kdht_service_expire_nodes(service, -1);
	return 0;
}


int kdht_cli_node_refresh(kli_t* cli, void *vcontext, kli_exec_t* exec)
{
	kdht_service_t *service = (kdht_service_t*)vcontext;

	kdht_service_refresh_nodes(service, -1);
	return 0;
}


int kdht_cli_node_replicate(kli_t* cli, void *vcontext, kli_exec_t* exec)
{
	kdht_service_t *service = (kdht_service_t*)vcontext;

	kdht_service_replicate_values(service, &service->rpc.table.local_values, -1);
	return 0;
}



int kdht_cli_node_garbagecollect_outstanding_calls(kli_t* cli, void *vcontext, kli_exec_t* exec)
{
	kdht_service_t *service = (kdht_service_t*)vcontext;

	kdht_service_gc_outstanding_calls(service, -1);
	return 0;
}

void kdht_cli_init(kdht_service_t *dht, kli_t *cli, kli_com_t *dht_command)
{
	kli_com_t *show = kli_register_command_allocs(cli, dht_command, "show", "show buckets", NULL, dht);

	kli_register_command_allocs(cli, show, "net", "show net", kdht_cli_show_active_net, dht);
	kli_register_command_allocs(cli, show, "buckets", "show buckets", kdht_cli_show_buckets, dht);
	kli_register_command_allocs(cli, show, "bucket", "show bucket <index>", kdht_cli_show_bucket_i, dht);
	kli_com_t *values = kli_register_command_allocs(cli, show, "values", "show values (local|remote)", NULL, dht);
	kli_register_command_allocs(cli, values, "local", "show values local", kdht_cli_show_values_local, dht);
	kli_register_command_allocs(cli, values, "remote", "show values remote", kdht_cli_show_values_remote, dht);


	kli_com_t *exec = kli_register_command_allocs(cli, dht_command, "exec", "exec <RPC>", NULL, dht);
	kli_register_command_allocs(cli, exec, "PING", "exec PING <host:port>", kdht_cli_ping_remote_str, dht);
	kli_register_command_allocs(cli, exec, "FIND_NODE", "exec FIND_NODE <IP> <ID>", kdht_cli_find_node_str, dht);
	kli_register_command_allocs(cli, exec, "STORE_VALUE", "exec STORE_VALUE <ID> <datahex>", kdht_cli_store_value_str, dht);
	kli_register_command_allocs(cli, exec, "FIND_VALUE", "exec FIND_VALUE <ID>", kdht_cli_find_value_str, dht);
	kli_register_command_allocs(cli, exec, "ITERATIVE_FIND_NODE", "exec ITERATIVE_FIND_NODE <ID>", kdht_cli_find_node_iterative_str, dht);
	kli_register_command_allocs(cli, exec, "ITERATIVE_FIND_VALUE", "exec ITERATIVE_FIND_VALUE <ID>", kdht_cli_find_value_iterative_str, dht);
	kli_register_command_allocs(cli, exec, "ITERATIVE_STORE_VALUE", "exec ITERATIVE_STORE_VALUE <ID>", kdht_cli_store_value_iterative_str, dht);
	kli_register_command_allocs(cli, exec, "EXPIRE", "exec EXPIRE", kdht_cli_node_expire, dht);
	kli_register_command_allocs(cli, exec, "REFRESH", "exec REFRESH", kdht_cli_node_refresh, dht);
	kli_register_command_allocs(cli, exec, "REPLICATE", "exec REPLICATE", kdht_cli_node_replicate, dht);
	kli_register_command_allocs(cli, exec, "GC", "exec GC", kdht_cli_node_garbagecollect_outstanding_calls, dht);

}
