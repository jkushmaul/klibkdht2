kdht2 
[![pipeline status](https://gitlab.com/jkushmaul/klibkdht2/badges/master/pipeline.svg)](https://gitlab.com/jkushmaul/klibkdht2/commits/master) 
[![coverage report](https://gitlab.com/jkushmaul/klibkdht2/badges/master/coverage.svg)](https://gitlab.com/jkushmaul/klibkdht2/commits/master)

This in no way is intended for use by others.  

My goal is to write an easy to use dht library, network agnostic, well tested (went out the door pretty quick), in C only for masochistic reasons.


* Source: https://gitlab.com/jkushmaul/klibkdht2
* Docs: https://jkushmaul.gitlab.io/klibkdht2/index.html
* Coverage: https://jkushmaul.gitlab.io/klibkdht2/coverage/index.html
* Releases: https://gitlab.com/api/v4/projects/3618443/releases
** Ask gitlab to implement a UI for it.


Using this as spec
http://xlattice.sourceforge.net/components/protocol/kademlia/specs.html

TODO:

  * TESTING
    Testing just flew out the window at a certain point :(
    Should test iterative find node, find value, and store value commands
    I mean proper testing with some harness that can run separate nodes in a controlled manner
    
  * USE OF RPC
    Very minor - The naming is off, but some of the iterative work really belongs in kdht_rpc and out of service.
    Service should just be an interface between network and rpc, nothing more really.

  * NODE KEEP ALIVE
    Unsure if this defeats the purpose - Routine to occasionally go out and get a ping/response if last activity from a node is outside a certain bound.  This is not refresh, its more
    about getting rid of offline nodes faster.  So an iterative find node isn't waiting on a large amount of requests that will not realize.

  * Standardize callback mechanism
     find node uses it's own method for find_node_calls (The whole operation), and each one outstanding_calls (The questions), as well as shortlist (The answers)
     findvalue uses its own with outstanding_findvalues and found_values_answers
     
     the find_node_calls are of type kdht_find_node_t
     the last two are of type kdht_value_t and a mix of hashmap and indexedhashmap
