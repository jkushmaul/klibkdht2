extern "C" {
#include <string.h>
#include <stdint.h>
#include <math.h>
}


#include "./tests.h"

extern "C" {
    #include <kdht_table.h>
}

TEST(kdht_table, kdht_table_insert_value) {
	kdht_table_t table = {};

	kdht_table_init_allocs(&table);
	kdht_value_t *value1 = (kdht_value_t*)calloc(1, sizeof(kdht_value_t));

	kdht_node_random(&value1->key);
	value1->expire_time = 1234;
	value1->last_replicate_time = 5678;
	char *data = (char*)calloc(1, 256);
	sprintf(data, "%s", "some data");
	value1->bytes = (uint8_t*)data;
	value1->len = 256;

	kdht_value_t *value = kdht_table_insert_value(&table.remote_values, value1);
	ASSERT_EQ((kdht_value_t*)NULL, value);
	ASSERT_EQ(1, table.remote_values.count);

	kdht_value_t *value2 = (kdht_value_t*)calloc(1, sizeof(kdht_value_t));
	memcpy(value2, value1, sizeof(kdht_value_t));
	value2->bytes = (uint8_t*)calloc(1, 256);
	memcpy(value2->bytes, data, 256);

	value = kdht_table_insert_value(&table.remote_values, value2);
	ASSERT_EQ(value1, value);
	ASSERT_EQ(1, table.remote_values.count);

	value1->key.id[1]++;
	value = kdht_table_insert_value(&table.remote_values, value1);
	ASSERT_EQ((kdht_value_t*)NULL, value);
	ASSERT_EQ(2, table.remote_values.count);
	kdht_table_release_frees(&table);
	test_free_list(&table.remote_values, 0);
}

TEST(kdht_table, kdht_table_get_value) {
	kdht_table_t table = {};

	kdht_table_init_allocs(&table);
	kdht_value_t *values[16] = { 0 };

	for (int i = 0; i < 16; i++) {
		values[i] = (kdht_value_t*)calloc(1, sizeof(kdht_value_t));
		values[i]->key.id[0] = i;
		kdht_table_insert_value(&table.remote_values, values[i]);
	}
	ASSERT_EQ(16, table.remote_values.count);
	for (int i = 0; i < 16; i++) {
		klist_node_t *node = kdht_table_get_value(&table.remote_values, &values[i]->key);
		ASSERT_NE((klist_node_t*)NULL, node);
		ASSERT_EQ(values[i], (kdht_value_t*)node->value);
	}
	kdht_node_t key;
	memset(&key, 0, sizeof(kdht_node_t));
	key.id[1] = 1;
	klist_node_t *node = kdht_table_get_value(&table.remote_values, &key);
	ASSERT_EQ((klist_node_t*)NULL, node);
	kdht_table_release_frees(&table);
	test_free_list(&table.remote_values, 0);
}

TEST(kdht_table, kdht_table_find_index){
	kdht_table_t table = {};
	kdht_node_t node2 = {};

	table.current_node.id[0] = 1;
	node2.id[0] = 0;//they will different at bit pos 7 of first byte
	int index = kdht_table_find_index(&table, &node2);
	ASSERT_EQ(7, index);
}

TEST(kdht_table, kdht_table_find_index2){
	kdht_table_t table = {};
	kdht_node_t node2 = {};

	table.current_node.id[2] = 1;
	node2.id[0] = 0;
	node2.id[1] = 1;//they will different at bit pos 7 of second byte, 8+7
	int index = kdht_table_find_index(&table, &node2);
	ASSERT_EQ(index, 15);
}

TEST(kdht_table, kdht_table_calculate_value_expire_time) {
	/*
	 *  C=( table index of key) * table[0 through index-1].bucket.count + index;
	            24 hours if C > k
	            else 24 *exp(k/C)
	 */
	kdht_table_t table = {};
	kdht_contact_t c = {};

	c.node.id[0] = 0xff;
	uint64_t t;
	t = kdht_table_calculate_value_expire_time(&table, &c);//find index == 0
	ASSERT_EQ(60 * 60 * 24, t);

	c.node.id[1] = 1;
	t = kdht_table_calculate_value_expire_time(&table, &c);//find index == K-1
	ASSERT_EQ(60 * 60 * 24, t);

	c.node.id[1] = 0;
	c.node.id[2] = 8;
	t = kdht_table_calculate_value_expire_time(&table, &c);//find index == K
	ASSERT_EQ(60 * 60 * 24, t);

	c.node.id[2] = 4;
	t = kdht_table_calculate_value_expire_time(&table, &c);//find index == K+1
	ASSERT_EQ(60 * 60 * 24 * exp(DHT_K / 25), t);


}

TEST(kdht_table, kdht_table_update_contact_allocs_frees__evics){
	kdht_table_t table = {};

	kdht_table_init_allocs(&table);

	//I want to populate the same bucket with table_update_node
	//so that DHT_K+1 is hit and one is evicted.
	kdht_contact_t *contacts[DHT_K + 1] = { 0 };
	int has_lt_dht_k = 0;
	int has_eq_dht_k = 0;

	for (int i = 0; i < DHT_K + 1; i++) {
		contacts[i] = (kdht_contact_t*)calloc(1, sizeof(kdht_contact_t));
		kdht_node_random(&contacts[i]->node);
		contacts[i]->node.id[0] = 0x80 | i;//make first bit 1

		int index = kdht_table_find_index(&table, &contacts[i]->node);
		ASSERT_EQ(0, index);

		kdht_table_update_contact_allocs_frees(100, &table, contacts[i]);
		if (i < DHT_K) {
			ASSERT_EQ(i + 1, table.routing_table[0].contacts_map.index.list.count);
			has_lt_dht_k++;
		} else {
			ASSERT_EQ(DHT_K, table.routing_table[0].contacts_map.index.list.count);
			has_eq_dht_k++;
		}
	}
	ASSERT_EQ(DHT_K, has_lt_dht_k);
	ASSERT_EQ(1, has_eq_dht_k);
	//as a sanity check make sure all other routing tables counts are 0.
	for (int i = 1; i < KDHT_TABLE_T_ELEMENTS; i++) {
		ASSERT_EQ(0, table.routing_table[i].contacts_map.index.list.count);
	}

	kdht_table_release_frees(&table);
}



TEST(kdht_table, kdht_table_find_n_close_nodes_allocs){
	kdht_table_t table = {};

	kdht_table_init_allocs(&table);

	kdht_contact_t *contacts[DHT_K] = { 0 };
	FOR_EACH_DHT_K(i) {
		contacts[i] = (kdht_contact_t*)calloc(1, sizeof(kdht_contact_t));
		contacts[i]->node.id[i] = 1;

		kdht_table_update_contact_allocs_frees(0, &table, contacts[i]);
	}
	kdht_node_t requestor;
	memset(&requestor, 0, sizeof(kdht_node_t));
	klist_t nnd = {};

	kdht_table_find_n_close_nodes_allocs(&table, &requestor, &contacts[3]->node, DHT_K, &nnd);
	ASSERT_EQ(DHT_K, nnd.count);

	kdht_table_release_frees(&table);
	test_free_list(&nnd, 0);
}


TEST(kdht_table, kdht_table_find_contact){
	kdht_table_t table = {};

	kdht_table_init_allocs(&table);

	kdht_contact_t *contacts[DHT_K] = { 0 };
	FOR_EACH_DHT_K(i) {
		contacts[i] = (kdht_contact_t*)calloc(1, sizeof(kdht_contact_t));
		contacts[i]->node.id[i] = 1;
		kdht_table_update_contact_allocs_frees(0, &table, contacts[i]);
	}
	kdht_node_t node;
	kdht_node_copy(&contacts[5]->node, &node);
	kdht_contact_t *contact = kdht_table_find_contact(&table, &node);
	ASSERT_EQ(contacts[5], contact);

	kdht_table_release_frees(&table);
}
