#include "./tests.h"

extern "C" {
    #include <kdht_node.h>
    #include <string.h>
}



TEST(kdht_node, copy){
	kdht_node_t src;
	kdht_node_t dst;

	memset(&src, 0, sizeof(kdht_node_t));
	memset(&dst, 0, sizeof(kdht_node_t));

	src.id[3] = 3;
	src.id[4] = 4;
	kdht_node_copy(&src, &dst);
	FOR_EACH_DHT_ID_BYTE(i) {
		ASSERT_EQ(src.id[i], dst.id[i]);
	}
}


TEST(kdht_node, equals){
	kdht_node_t src;
	kdht_node_t dst;

	memset(src.id, 0, sizeof(kdht_node_t));
	memset(dst.id, 0, sizeof(kdht_node_t));

	src.id[3] = 3;
	src.id[4] = 4;
	ASSERT_NE(0, kdht_node_equals(&src, &dst));

	dst.id[3] = 3;
	dst.id[4] = 4;
	ASSERT_EQ(0, kdht_node_equals(&src, &dst));
}

TEST(kdht_node, tostring){
	kdht_node_t node;
	kdht_node_string_t expected;

	memset(&expected, 0, sizeof(expected));

	char *buff = expected.id;
	for (int i = 0; i < DHT_K; i++) {
		node.id[i] = i;
		snprintf(buff, 3, "%02x", i);
		buff += 2;
	}
	//sanity check on expected value, not testing anything
	ASSERT_EQ(0, strcmp("000102030405060708090a0b0c0d0e0f10111213", expected.id));
	kdht_node_string_t actual;
	kdht_node_tostring(&node, &actual);
	ASSERT_EQ(0, strcmp(expected.id, actual.id));
}

TEST(kdht_node, parsestring){
	kdht_node_string_t node = { .id = "000102030405060708090a0b0C0d0e0f10111213" };//upper and lower hex
	kdht_node_t expected;

	for (int i = 0; i < DHT_K; i++) {
		expected.id[i] = i;
	}
	kdht_node_t actual;
	int result = kdht_node_parse(&node, &actual);
	ASSERT_EQ(0, result);
	for (int i = 0; i < DHT_K; i++) {
		ASSERT_EQ(expected.id[i], actual.id[i]);
	}
}

TEST(kdht_node, parse_fail_length){
	kdht_node_string_t node = { .id = "000102030405060708091011121314151617181\0" };//bad length
	kdht_node_t actual;
	int result = kdht_node_parse(&node, &actual);

	ASSERT_EQ(-1, result);
}

TEST(kdht_node, parse_fail_character){
	kdht_node_string_t node = { .id = "000102030405060708091011121314151617181G" };//G is not hex
	kdht_node_t actual;
	int result = kdht_node_parse(&node, &actual);

	ASSERT_NE(0, result);
}


TEST(kdht_node, random){
	kdht_node_t r;

	memset(&r, 0, sizeof(kdht_node_t));
	kdht_node_random(&r);
	kdht_node_t r2;
	memset(&r2, 0, sizeof(kdht_node_t));
	kdht_node_random(&r2);
	//not very deterministic
	ASSERT_NE(0, kdht_node_equals(&r, &r2)); //very slim chance, this *is* non-deterministic though...
}


TEST(kdht_node, most_sig_bit_pos__no_bits_set){
	kdht_node_t n;

	int actual;
	int expected;

	expected = DHT_BITS - 1;
	memset(&n, 0, sizeof(kdht_node_t ));
	actual = kdht_node_most_sig_bit_pos(&n);
	ASSERT_EQ(expected, actual);
}

TEST(kdht_node, most_sig_bit_pos__first_bit_set){
	kdht_node_t n;

	int actual;
	int expected;

	expected = 0;
	memset(&n, 0xff, sizeof(kdht_node_t ));
	actual = kdht_node_most_sig_bit_pos(&n);
	ASSERT_EQ(expected, actual);
}

TEST(kdht_node, most_sig_bit_pos__last_bit_second_index){
	kdht_node_t n;

	int actual;
	int expected;

	expected = 15;
	memset(&n, 0, sizeof(kdht_node_t ));
	n.id[1] = 0x01;
	actual = kdht_node_most_sig_bit_pos(&n);
	ASSERT_EQ(expected, actual);
}

TEST(kdht_node, calculate_distance) {
	kdht_node_t left, right, dist, expected;

	memset(&left, 0, sizeof(kdht_node_t ));
	memset(&right, 0, sizeof(kdht_node_t ));
	memset(&expected, 0, sizeof(kdht_node_t));
	left.id[0] = 0x0a;
	right.id[0] = 0x0a;
	left.id[1] = 0xff;
	expected.id[1] = 0xff;
	kdht_node_calculate_distance(&left, &right, &dist);
	FOR_EACH_DHT_ID_BYTE(i) {
		ASSERT_EQ(expected.id[i], dist.id[i]);
	}
}
