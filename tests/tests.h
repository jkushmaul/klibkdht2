#ifndef TESTS_H
#define TESTS_H
#include <atomic>

using namespace std;

#include <gtest/gtest.h>

extern "C" {
#include <klist.h>
#include <kdht_table.h>
}


void inline test_free_list(klist_t *nnd, int freevalue)
{
    klist_node_t *n = 0;
    n= nnd->head;
    while (n) {
        klist_node_t *thisnode = n;
        n = n->next;
        klist_remove_node(nnd, thisnode);
        if (freevalue) {
            free(thisnode->value);
        }
        free(thisnode);
    }
}


#endif
