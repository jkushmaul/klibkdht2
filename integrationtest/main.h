#ifndef MYCLI_H
#define MYCLI_H
#include <string.h>
#include <stdio.h>
#include <pthread.h>
#include <signal.h> 
#include <unistd.h>

#include <kli.h>
#include <kencode.h>
#include <konfig2.h>   
#include <kdht_service.h>

#include <kdht_cli.h>


#undef KSERVICE_LOCK
#define KSERVICE_LOCK(mutex) pthread_mutex_lock(&mutex);

typedef struct {
    kdht_service_t service;
    konfig2_t konfig;
    kli_t *cli;
  //  char *command;
  //  pthread_mutex_t mutex;
} context_t;





int mykli_set_config(kli_t* cli, void* vcontext, kli_exec_t *exec) {
    context_t *context = (context_t*) vcontext;
    
    if (exec->argc != 2) {
        printf("Two arguments required:  <name> <value>\n");
        return -1;
    }
    
    char *name = exec->argv[0];
    char *value = exec->argv[1];
    
    konfig2_var_t *var = konfig2_get_var(&context->konfig, name);
    if (var == NULL) {
        printf("Could not find %s konfig var\n", name);
        return -1;
    } else {
        if (var->parse(var, value)) {
            printf("Invalid value '%s' for var %s\n", value, name);
        } else {
            printf("var %s was set successfully\n", name);
        }
    }
    
    return 0;
}

int mykli_show_config(kli_t* cli, void *vcontext, kli_exec_t* exec) {
    context_t *context = (context_t*) vcontext;
    
    char *buffer = konfig2_dump(&context->konfig);
    if (buffer) {
        printf("%s\n", buffer);
    } else {
        printf("Error getting konfig2\n");
    }
    return 0;
}

int mykli_quit(kli_t* cli, void *vcontext, kli_exec_t* exec) {
    knet2_error_t error = { 0 };
    
    context_t *context = (context_t*) vcontext;
    kdht_service_t *service = &context->service;
    
    cli->shutdown = 1;
    service->rpc.service.should_run = 0;
    knet2_interrupt(context->service.rpc.service.network_handler, &error);
    
    return 0;
}

int mykli_clear(kli_t* cli, void *vcontext, kli_exec_t* exec) {
    void linenoiseClearScreen(void);
    return 0;
}

int mykli_help(kli_t* cli, void *vcontext, kli_exec_t* exec) {
    if (cli != NULL) {
        kli_recurse_print_commands(cli, &cli->root_command, 0);    
    }
    
    return 0;
}




void mykli_init(kli_t* cli, context_t *context) {

    kli_init(cli);
    
    kli_register_command_allocs(cli, 0, "help", "help", mykli_help, context);
    kli_register_command_allocs(cli, 0, "quit", "quit", mykli_quit, context);
    kli_register_command_allocs(cli, 0, "clear", "clear", mykli_clear, context); 
    
    
    kli_com_t *set = kli_register_command_allocs(cli, 0, "set", "set", NULL, NULL);
    kli_register_command_allocs(cli, set, "config", "set config <refresh_seconds|network_timeout_seconds|replicate_seconds|republish_seconds>", mykli_set_config, context);
    
    kli_com_t *show = kli_register_command_allocs(cli, 0, "show", "show buckets", NULL, NULL);
    kli_register_command_allocs(cli, show, "config", "show config", mykli_show_config, context);

    
    kdht_cli_init(&context->service, cli, NULL);
}



#endif
