#define _GNU_SOURCE
#include <pthread.h>

#include <stdio.h>
#include <stdlib.h>
#include <knet2_epoll.h>
#include <signal.h>
#include <time.h>
#include <errno.h>



#include <linenoise/linenoise.h>

#include <kdht_service.h>
#include <kdht_node.h>
#include <kdht_log.h>
#include "main.h"
#include <karg.h>
#include <kdht_node.h>
#include <kli.h>
static karg_option_t public_network_address;
static karg_option_t listen_address;
static karg_option_t seed_address;
static karg_option_t local_node;

int parse_args(karg_t *args, int argc, char **argv) {
    karg_init(args);
    strcpy(args->author, "jkushmaul");
    
    karg_option_init(&args->options, &public_network_address, 'p', (char*)"public", (char*)"Public address for responses", 1, NULL, 1);
    karg_option_init(&args->options, &seed_address, 's', (char*)"seed", (char*)"Seed address", 1, NULL, 1);
    karg_option_init(&args->options, &listen_address, 'l', (char*) "listen", (char*) "Local address to listen. If not provided uses public",0, NULL, 1);
    karg_option_init(&args->options, &local_node, 'n', (char*) "node", (char*) "Local node id.  If not provided is generated",0, NULL, 1);
    
    karg_getopt_option_def *def = karg_build_getopts_allocs(args);
    int parse = karg_parse_args(args, def, argc, argv);
    free(def);
    if (parse < 0) {
        return -1;
    }
    int test = karg_validate_args(args);
    
    if(test) {
        return -1;
    } else {
        return 0;
    }
}

int setup_konfig(context_t *context) 
{
    konfig2_init_allocs(&context->konfig, 1000);
    konfig2_var_new_long_allocs(&context->konfig, "refresh_seconds", &context->service.config.refresh_seconds);
    konfig2_var_new_long_allocs(&context->konfig, "network_timeout_seconds", &context->service.config.network_timeout_seconds);
    konfig2_var_new_long_allocs(&context->konfig, "expire_seconds", &context->service.config.expire_seconds);
    konfig2_var_new_long_allocs(&context->konfig, "replicate_seconds", &context->service.config.replicate_seconds);
    konfig2_var_new_long_allocs(&context->konfig, "republish_seconds", &context->service.config.republish_seconds);
    
    context->service.config.replicate_seconds = 3600;
    context->service.config.republish_seconds = 87400;
    context->service.config.expire_seconds = 3600;
    context->service.config.refresh_seconds = 60;
    context->service.config.network_timeout_seconds = 1;
    return 0;
}


void mykli_process_thread_input(kservice_t *service, void *vcontext)
{
    context_t *context = (context_t*) vcontext;
    kli_process_thread_input(context->cli);
}

int main(int argc, char **argv) 
{
    karg_t args;
    if (parse_args(&args, argc, argv)) {
        karg_print_help(&args);
        exit(-1);
    }
    
    const char *local_ip = listen_address.value;
    const char *public_ip = public_network_address.value;
    if (local_ip == NULL) {
        local_ip = public_ip;
    }
    const char *remote_ip = seed_address.value;
    


    kdht_node_string_t actual_node_str;
    memset(&actual_node_str, 0, sizeof(actual_node_str));
    
    if (local_node.value == NULL) {
        //srand (time(NULL));
        //uint8_t b = rand() % 255;
        
        //const char *nodestr="0104e7f05ee97be1fd050498bc31946c01fb84"; //-1 hex byte
        //snprintf(actual_node_str.id, sizeof(actual_node_str), "%s%02x", nodestr, b);
        kdht_node_t tmp;
        kdht_node_random(&tmp);
        kdht_node_tostring(&tmp, &actual_node_str);
    } else if (strlen(local_node.value) != DHT_ID_BYTES * 2) {
        printf("Invalid value for local node\n");
        karg_print_help(&args);
        exit(-1); 
    } else {
        snprintf(actual_node_str.id, sizeof(actual_node_str), "%s", (char*) local_node.value);
    }
    
    printf("Starting node %s on ip %s\n", actual_node_str.id, public_ip);
    context_t context;
    memset(&context, 0, sizeof(context_t));
    
    setup_konfig(&context);
    
    context.service.rpc.contact_information.network_address = (char*) public_ip;
    
    if(kdht_node_parse(&actual_node_str, &context.service.rpc.contact_information.node)!=0) {
        printf("error parsing %s\n", actual_node_str.id);
        karg_print_help(&args);
        exit(-1);
    }
    
    knet2_error_t knet2error = { 0 };
    
    //init network
    knet2_epoll_handler_t tcp;
    knet2_epoll_handler_init_allocs(&tcp, 1000, &knet2error);
    tcp.ip_base.listen_backlog=4096;
    tcp.ip_base.addr_parse(&tcp.ip_base.this_addr, local_ip, &knet2error);

    kdht_service_init((kdht_service_t*)&context, (knet2_ip_handler_t*)&tcp);
    if (kservice_start((kservice_t*)&context, &knet2error)){
        klog_msg(ERROR, "Could not start service");
        exit(-1);
    }
    
    kli_t *mycli = (kli_t*) calloc(1, sizeof(kli_t));
    mykli_init(mycli, &context);
    context.cli = mycli;
    
    if (kli_start_input_thread(mycli)) {
        printf("Could not create cli input thread\n");
        exit(-1);
        
    }
    kservice_add_loop_complete_callback((kservice_t*) &context, &context, mykli_process_thread_input);
    
    
    //bootstrap
    kdht_service_bootstrap_allocs((kdht_service_t*)&context, &remote_ip, 1);
    
    //loop will shut itself down
    kservice_loop((kservice_t*)&context, &knet2error);
    
    printf("Service loop done\n");
    kli_shutdown(mycli);
    free(mycli);
   
    
    kdht_service_release_frees((kdht_service_t*)&context.service.rpc.service);
    context.service.rpc.service.network_handler->release_addr(context.service.rpc.service.network_handler, &context.service.rpc.service.network_handler->this_addr);
    karg_release_frees(&args);
    konfig2_release_frees(&context.konfig);
    
    printf("\n");
    return 0;
}
