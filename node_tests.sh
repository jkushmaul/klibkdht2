#!/bin/bash
PROG="$0"
function die() {
   echo "usage: $0 <local.last.octet> <seed.last.octet>"
   exit -1
}

function start_test_terminal() {
    test "$1" != "" || die 
    test "$2" != "" || die
    p="$1"
    s="$2"
    cmd="valgrind --leak-check=full ./kdht2cli -p127.0.0.$p:31337 -s127.0.0.$s:31337"
    echo "Launching $cmd"
    gnome-terminal -t "test $p"  -- bash -c "$cmd; read"
}

COUNT="$1"
if [ "$COUNT" == "" ]; then
  COUNT="1"
fi
if [ $COUNT -gt 253 ]; then
  echo "COUNT($COUNT) Must be less than 253"
  exit -1
fi

#seed already listens on 1
for ((i = 0; i < $COUNT; ++i)) {
  start_test_terminal $(($i+2)) 1
  sleep 1 
} 
